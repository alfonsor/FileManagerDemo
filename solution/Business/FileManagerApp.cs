﻿using Commons;
using System;

namespace Business
{
    public class FileManagerApp
    {
        private readonly IFileManager _fileManager;

        public FileManagerApp(IFileManager fileManager)
        {
            _fileManager = fileManager;
        }

        public string ProcessFile(string path)
        {
            if (_fileManager.CanRead(path))
            {
                if (path.Length > 50)
                {
                    throw new FileManagerException("Error!!!!");
                }

                string content = _fileManager.Read(path);

                return content;
            }

            return null;
        }
    }
}
