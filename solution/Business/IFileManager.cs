﻿namespace Business
{
    public interface IFileManager
    {
        bool CanRead(string path);

        string Read(string path);
    }
}
