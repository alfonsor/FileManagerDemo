﻿using Commons;

namespace Business
{
    public class TextFileManager : IFileManager
    {
        public bool CanRead(string path)
        {
            if (string.IsNullOrEmpty(path)) return false;

            if (!System.IO.File.Exists(path)) return false;

            return true;
        }

        public string Read(string path)
        {
            string content = System.IO.File.ReadAllText(path);

            return content;
        }
    }
}
