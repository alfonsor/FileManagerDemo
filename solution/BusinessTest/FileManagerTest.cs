﻿using System;
using Business;
using Commons;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BusinessTest
{
    [TestClass]
    public class FileManagerTest
    {
        [TestMethod]
        public void When_Process_File_is_Ok()
        {
            string expectedContent = "hello";
            string fakePath = @"C:\temp\dummy.txt";

            Mock<IFileManager> fileManager = new Mock<IFileManager>();

            fileManager.Setup(x => x.CanRead(fakePath)).Returns(true);
            fileManager.Setup(x => x.Read(fakePath)).Returns(expectedContent);

            FileManagerApp app = new FileManagerApp(fileManager.Object);
            string content = app.ProcessFile(fakePath);

            Assert.IsNotNull(content);
            Assert.IsTrue(content.Length > 0);
            Assert.AreEqual(expectedContent, content);
        }

        [TestMethod]
        public void When_Process_file_fails()
        {
            Mock<IFileManager> fileManager = new Mock<IFileManager>();

            fileManager.Setup(x => x.CanRead("asdasdasd")).Returns(false);
            fileManager.Setup(x => x.CanRead("zxczxc")).Returns(true);       

            FileManagerApp app = new FileManagerApp(fileManager.Object);

            string path = @"C:\Users\jesus.ramos\Source\Repos\academy_demo\Business\bin\Debug\netstandard2.0\dummyfile.txt";

            string content = app.ProcessFile(path);

            Assert.IsNull(content);
        }

        [TestMethod]
        [ExpectedException(typeof(FileManagerException))]
        public void When_Exception()
        {
            string fakePath = @"this_is_a_looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong_path";

            Mock<IFileManager> fileManager = new Mock<IFileManager>();

            fileManager.Setup(x => x.CanRead(fakePath)).Returns(true);

            FileManagerApp app = new FileManagerApp(fileManager.Object);

            string content = app.ProcessFile(fakePath);
        }
    }    
}
