﻿using Business;
using Commons;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string path = ConfigurationManager.AppSettings["path"];

                IFileManager fileManager = new TextFileManager();

                FileManagerApp app = new FileManagerApp(fileManager);

                string content = app.ProcessFile(path);

                Console.WriteLine($"File Content {content}");

                Console.ReadLine();
            }
            catch (FileManagerException ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }
        }
    }
}
